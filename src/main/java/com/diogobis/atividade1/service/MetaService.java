package com.diogobis.atividade1.service;

import java.util.List;
import java.util.Optional;

import com.diogobis.atividade1.domain.Meta;
import com.diogobis.atividade1.repository.MetaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MetaService {
    private final Logger log = LoggerFactory.getLogger(MetaService.class);

    private final MetaRepository metaRepository;

    public MetaService(MetaRepository metaRepository){
        this.metaRepository = metaRepository;
    }

    public List<Meta> findAllList(){
        return metaRepository.findAll();
    }

    public Optional<Meta> findOne(Long id){
        log.debug("Request to get Meta : {}", id);
        return metaRepository.findById(id);
    }

    public void delete(long id){
        log.debug("Request to delete Meta : {}", id);
        metaRepository.deleteById(id);
    }

    public Meta save(Meta meta){
        log.debug("Request to save Meta : {}");
        meta = metaRepository.save(meta);
        return meta;
    }
}
