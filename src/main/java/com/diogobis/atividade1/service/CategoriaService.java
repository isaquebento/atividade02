package com.diogobis.atividade1.service;

import java.util.List;
import java.util.Optional;

import com.diogobis.atividade1.domain.Categoria;
import com.diogobis.atividade1.repository.CategoriaRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CategoriaService {
    private final Logger log = LoggerFactory.getLogger(CategoriaService.class);

    private final CategoriaRepository categoriaRepository;

    public CategoriaService(CategoriaRepository categoriaRepository){
        this.categoriaRepository = categoriaRepository;
    }

    public List<Categoria> findAllList(){
        return categoriaRepository.findAll();
    }

    public Optional<Categoria> findOne(Long id){
        log.debug("Request to get Categoria : {}", id);
        return categoriaRepository.findById(id);
    }

    public void delete(long id){
        log.debug("Request to delete Categoria : {}", id);
        categoriaRepository.deleteById(id);
    }

    public Categoria save(Categoria categoria){
        log.debug("Request to save Categoria : {}");
        categoria = categoriaRepository.save(categoria);
        return categoria;
    }
}
