package com.diogobis.atividade1.repository;

import com.diogobis.atividade1.domain.Meta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MetaRepository extends JpaRepository<Meta, Long>{
    
}
