package com.diogobis.atividade1.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "table_livro")
public class Livro {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="titulo", length = 64)
    private String titulo;

    private String descricao;
    private String capa;
    private int numPaginas;
    private String autor;
    private String editora;

    private boolean lido;
    private Date dataLido;

    //relação entre jogos e usuários
    @ManyToOne
    @JoinColumn(name="usuario_id")
    private Usuario usuario; //segundo o site aqui é tipo Usuario, mas não seria long?

    @ManyToOne
    @JoinColumn(name="categoria_id")
    private Categoria categoria;
}
