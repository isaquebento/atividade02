package com.diogobis.atividade1.domain;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "table_usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome", length = 64)
    private String nome;

    @Column(name = "senha", length = 24)
    private String senha;

    private String email;
    private String cpf;
    private String foto;
    private Date dataNasc;

    private String celular;
    private int sexo;

    private boolean isActive;

    //relação entre usuários e livros
    @OneToMany(mappedBy = "usuario")
    private List<Livro> livros;

    //relação entre usuários e categorias
    @OneToMany(mappedBy = "usuario")
    private List<Categoria> categorias;

    //relação entre usuários e metas
    @OneToMany(mappedBy = "usuario")
    private List<Meta> metas;
}
