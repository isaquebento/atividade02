package com.diogobis.atividade1.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import com.diogobis.atividade1.domain.Categoria;
import com.diogobis.atividade1.service.CategoriaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/categorias")
public class CategoriaResource {
    private final Logger log = LoggerFactory.getLogger(CategoriaResource.class);

    private final CategoriaService categoriaService;

    public CategoriaResource(CategoriaService categoriaService){
        this.categoriaService = categoriaService;
    }

    // @GetMapping(path="/listar")
    // public List<Categoria> helloApp(){
    //     List<Categoria> lista = categoriaService.findAllList();
    //     return lista;
    // }

    // @GetMapping(path="/criar/{name}")
    // public String helloApp(@PathVariable String name){
    //     Categoria j = new Categoria();
    //     j.setTitulo(name);
    //     categoriaService.save(j);
    //     return "Jogo criado<br>Nome: "+name;
    // }

    // @GetMapping(path="/{id}")
    // public Categoria helloApp(@PathVariable long id){
    //     return categoriaService.findOne(id).get();
    // }

    /*
    {@code GET /categorias/:id} : get the "id" Categoria.

    @param id o id do usuário que será buscado.
    @return o {@link ResponseEntity} com status {@code 200 (OK)} e no body o usuário, ou com status {@code 404 (Not Found)}.
    */
    @GetMapping(path="/{id}")
    public ResponseEntity<Categoria> getCategoria(@PathVariable Long id){
        log.debug("REST request to get Categoria : {}",id);
        Optional<Categoria> categorias = categoriaService.findOne(id);
        if(categorias.isPresent()){
            return ResponseEntity.ok().body(categorias.get());
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path="/")
    public ResponseEntity<List<Categoria>> getCategorias(){
        List<Categoria> lista = categoriaService.findAllList();
        if (lista.size() > 0){
            return ResponseEntity.ok().body(lista);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    /*
    {@code PUT /categorias} : Updates an existing Categoria.

    @param categorias the user that is to be updated.
    @return the {@link ResponseEntity} with status {@code 200 (OK)} and with the update Categoria in the body,
    or with status {@code 400 (Bad Request)} if categorias is not valid,
    or with status {@code 500 (Internal Server Errror)} if categorias can't be updated.

    @throws URISyntaxException if the Location URI syntax is incorrect
    */
    @PutMapping("/")
    public ResponseEntity<Categoria> updateCategoria(@RequestBody Categoria categorias) throws URISyntaxException {
        log.debug("REST request to update Categoria : {}",categorias);
        if(categorias.getId() == null){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Invalid Categoria id null");
        }
        Categoria result = categoriaService.save(categorias);
        return ResponseEntity.ok().body(result);
    }


    /*
    {@code POST /} : create a new Categoria.

    @param categorias the pessoa to create.
    @return the {@link ResponseEntity} with status {@code 200 (OK)} eand with body the new Categoria, or with status {@code 404 (Not Found)}.
    @throws URISyntaxException if the Location URI syntax is incorrect
    */
    @PostMapping("/")
    public ResponseEntity<Categoria> createCategoria(@RequestBody Categoria categorias) throws URISyntaxException{
        log.debug("REST request to save Categoria : {}",categorias);
        if (categorias.getId() != null){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Um novo usuário não pode ter um ID");
        }
        Categoria result = categoriaService.save(categorias);
        return ResponseEntity.created(new URI("/api/categorias/"+ result.getId())).body(result);
    }
    

    /*
    {@code DELETE /:id} delete Categoria with "id".

    @param id the id of the Categoria that is going to be deleted
    @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}
    */

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategoria(@PathVariable Long id){
        log.debug("REST request to delete Categoria : {}", id);

        categoriaService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
