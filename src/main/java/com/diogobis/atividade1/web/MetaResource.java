package com.diogobis.atividade1.web;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import com.diogobis.atividade1.domain.Meta;
import com.diogobis.atividade1.service.MetaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/metas")
public class MetaResource {
    private final Logger log = LoggerFactory.getLogger(MetaResource.class);

    private final MetaService metaService;

    public MetaResource(MetaService metaService){
        this.metaService = metaService;
    }

    // @GetMapping(path="/listar")
    // public List<Meta> helloApp(){
    //     List<Meta> lista = metaService.findAllList();
    //     return lista;
    // }

    // @GetMapping(path="/criar/{name}")
    // public String helloApp(@PathVariable String name){
    //     Meta j = new Meta();
    //     j.setTitulo(name);
    //     metaService.save(j);
    //     return "Jogo criado<br>Nome: "+name;
    // }

    // @GetMapping(path="/{id}")
    // public Meta helloApp(@PathVariable long id){
    //     return metaService.findOne(id).get();
    // }

    /*
    {@code GET /metas/:id} : get the "id" Meta.

    @param id o id do usuário que será buscado.
    @return o {@link ResponseEntity} com status {@code 200 (OK)} e no body o usuário, ou com status {@code 404 (Not Found)}.
    */
    @GetMapping(path="/{id}")
    public ResponseEntity<Meta> getMeta(@PathVariable Long id){
        log.debug("REST request to get Meta : {}",id);
        Optional<Meta> metas = metaService.findOne(id);
        if(metas.isPresent()){
            return ResponseEntity.ok().body(metas.get());
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(path="/")
    public ResponseEntity<List<Meta>> getMetas(){
        List<Meta> lista = metaService.findAllList();
        if (lista.size() > 0){
            return ResponseEntity.ok().body(lista);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    /*
    {@code PUT /metas} : Updates an existing Meta.

    @param metas the user that is to be updated.
    @return the {@link ResponseEntity} with status {@code 200 (OK)} and with the update Meta in the body,
    or with status {@code 400 (Bad Request)} if metas is not valid,
    or with status {@code 500 (Internal Server Errror)} if metas can't be updated.

    @throws URISyntaxException if the Location URI syntax is incorrect
    */
    @PutMapping("/")
    public ResponseEntity<Meta> updateMeta(@RequestBody Meta metas) throws URISyntaxException {
        log.debug("REST request to update Meta : {}",metas);
        if(metas.getId() == null){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Invalid Meta id null");
        }
        Meta result = metaService.save(metas);
        return ResponseEntity.ok().body(result);
    }


    /*
    {@code POST /} : create a new Meta.

    @param metas the pessoa to create.
    @return the {@link ResponseEntity} with status {@code 200 (OK)} eand with body the new Meta, or with status {@code 404 (Not Found)}.
    @throws URISyntaxException if the Location URI syntax is incorrect
    */
    @PostMapping("/")
    public ResponseEntity<Meta> createMeta(@RequestBody Meta metas) throws URISyntaxException{
        log.debug("REST request to save Meta : {}",metas);
        if (metas.getId() != null){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Um novo usuário não pode ter um ID");
        }
        Meta result = metaService.save(metas);
        return ResponseEntity.created(new URI("/api/metas/"+ result.getId())).body(result);
    }
    

    /*
    {@code DELETE /:id} delete Meta with "id".

    @param id the id of the Meta that is going to be deleted
    @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}
    */

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteMeta(@PathVariable Long id){
        log.debug("REST request to delete Meta : {}", id);

        metaService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
